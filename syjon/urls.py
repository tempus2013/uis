# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns, include
from django.contrib import admin

admin.autodiscover()

uis_patterns = ['']
for app_name in settings.INSTALLED_APPS:
    app_details = app_name.split('.')
    if app_details[0] == 'apps' and app_details[1] != 'syjon':
        urls_module = '{0}.urls'.format(app_name)
        try:
            __import__(urls_module)
        except ImportError:
            continue
        uis_patterns.append((r'^{0}/'.format(app_details[1]), include(urls_module)))

urlpatterns = patterns(
    '',
    (r'^admin/', include(admin.site.urls)),
    (r'^i18n/', include('django.conf.urls.i18n')),
)

if len(uis_patterns) > 0:
    urlpatterns += patterns(*uis_patterns)

urlpatterns += patterns(
    '',
    (r'^', include('apps.syjon.urls')),
)