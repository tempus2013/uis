# -*- coding: utf-8 -*-

import os
import config

# ------------------------------------------------------
# --- MISC
# ------------------------------------------------------

IS_READ_ONLY = config.IS_READ_ONLY

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))

ADMINS = ()
MANAGERS = ADMINS

SITE_ID = 1

ROOT_URLCONF = 'syjon.urls'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

DJANGO_SETTINGS_MODULE = 'syjon.settings'
WSGI_APPLICATION = 'syjon.wsgi.application'

CONTACT_INFO = config.CONTACT_INFO

# ------------------------------------------------------
# --- EMAIL
# ------------------------------------------------------

EMAIL_HOST = config.EMAIL_HOST
EMAIL_PORT = config.EMAIL_PORT
EMAIL_HOST_USER = config.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = config.EMAIL_HOST_PASSWORD
DEFAULT_FROM_EMAIL = config.DEFAULT_FROM_EMAIL
EMAIL_USE_TLS = config.EMAIL_USE_TLS

# ------------------------------------------------------
# --- DATABASES
# ------------------------------------------------------

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config.DB_NAME,                            
        'USER': config.DB_USER,                                
        'PASSWORD': config.DB_PASS,                       
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
}

# ------------------------------------------------------
# --- SECURITY
# ------------------------------------------------------

DEBUG = config.DEBUG
TEMPLATE_DEBUG = DEBUG

INTERNAL_IPS = ('127.0.0.1',)  # Tupla z zaufanymi adresami IP

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.SHA1PasswordHasher',  # Insecure Hashes
)

AUTH_PROFILE_MODULE = 'trainman.UserProfile'
LOGIN_URL = config.SITE_URL + 'trainman/login/'

SECRET_KEY = '734!1@yrn24zd2jp3c47ok8$be0saeqc&*m!z+bhwv(0cmj1ht'  # Make this unique, and don't share it with anybody.

# ------------------------------------------------------
# --- LANGUAGES
# ------------------------------------------------------

USE_I18N = True
USE_L10N = True
USE_TZ = True

TIME_ZONE = 'Europe/Warsaw'

gettext = lambda s: s

LANGUAGE_CODE = 'pl'

LANGUAGES = [('pl', gettext('polish')),
             ('en', gettext('english')),
             ('ua', gettext('ukrainian')),
             ('ru', gettext('russian')), ]


# ------------------------------------------------------
# --- STATIC AND MEDIA FILES
# ------------------------------------------------------

MEDIA_ROOT = PROJECT_PATH + '/media/'
MEDIA_URL = '/media/'

STATIC_ROOT = PROJECT_PATH + '/static/'
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_DIRS = ()


# ------------------------------------------------------
# --- CACHE
# ------------------------------------------------------

CACHE_BACKEND = 'file:///var/tmp/django_cache'
CACHE_MIDDLEWARE_SECONDS = 60*15
CACHE_MIDDLEWARE_KEY_PREFIX = ''

# ------------------------------------------------------
# --- TEMPLATES
# ------------------------------------------------------

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'apps.syjon.context_processors.consts',
)

TEMPLATE_DIRS = ()

# ------------------------------------------------------
# --- MIDDLEWARE CLASSES
# ------------------------------------------------------

MIDDLEWARE_CLASSES = (
    'apps.syjon.middleware.ForceDefaultLanguageMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

# ------------------------------------------------------
# --- INSTALED APPS
# ------------------------------------------------------

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
)  
    
INSTALLED_APPS += (
    'apps.syjon',
    'apps.trainman',
    'apps.megacity',
    'apps.merovingian',
    'apps.pythia',
    'apps.trinity',
    'apps.whiterabbit',
    'apps.metacortex',
    'apps.niobe',
    'apps.booth',
    'apps.link',
    'apps.lock',
    'apps.orb',
    'apps.morpheus',
    'apps.johnson',
)

INSTALLED_APPS += (
    'modeltranslation',
    'south',
    'tinymce'
)

# ------------------------------------------------------
# --- MODEL TRANSLATION
# ------------------------------------------------------

MODELTRANSLATION_TRANSLATION_FILES = (
    'apps.trinity.translation',
    'apps.metacortex.translation',
    'apps.merovingian.translation',
)

MODELTRANSLATION_DEBUG = config.DEBUG
