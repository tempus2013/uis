# -*- coding: utf-8 -*-

from apps.trainman.models import Department, UserProfile
from apps.merovingian.models import Course
from apps.morpheus.models import MorpheusProfile

profiles = {
  25: [697, 1086, 97], # Wydział Artystyczny
  24: [1352, 677, 82], # Wydział Biologii i Biotechnologii
  23: [860, 1375, 1311, 1000], # Wydział Chemii
  22: [431, 2016, 1257, 1158], # Wydział Ekonomiczny
  8: [161, 613, 967, 296], # Wydział Humanistyczny
  2: [407, 2134], # Wydział Matematyki, Fizyki i Informatyki
  27: [308, 89, 1347], # Wydział Nauk o Ziemi i Gospodarki Przestrzennej
  15: [1467, 744, 1974, 138], # Wydział Pedagogiki i Psychologii
  13: [2650, 1820, 328], # Wydział Politologii
  16: [2300, 1197, 1098], # Wydział Filozofii i Socjologii
  60: [1212, 2341, 380, 1316, 610], # Wydział Prawa i Administracji
}

for department_id, user_profiles_ids in profiles.iteritems():
  department = Department.objects.get(id=department_id)
  department_children = department.children_id()
  courses = Course.objects.filter(department__id__in = department_children)
  for user_profile_id in user_profiles_ids:
    user_profile = UserProfile.objects.get(id = user_profile_id)
    profile, created = MorpheusProfile.objects.get_or_create(user_profile = user_profile)
    profile.courses.clear()    
    for course in courses:
      profile.courses.add(course)

  



